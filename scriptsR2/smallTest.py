configurePIDCalibTupleProduction(
      DataType = "2015"
      , TupleFile = "tuple.root"
      , Simulation = False
      , Lumi       = False
      , Stream     = "Turbo"
      , InputType = 'DST'
      , EvtMax    = 10000
      , mdstOutputFile = "PID"
      , mdstOutputPrefix = "00000"
      , sTableFile = "../../sTables/sPlotTables-15MagDown.root"
   )

