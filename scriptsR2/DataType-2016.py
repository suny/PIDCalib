from Configurables import DaVinci
import os

DaVinci().DataType = '2016'

from Configurables import SWeightsTableFiles
SWeightsTableFiles(sTableMagUpFile  = os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2016MagUp.root', 
                   sTableMagDownFile= os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2016MagDown.root') 


