###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from PidCalibProduction.Run2 import parseConfigurationNeutral
from PidCalibProduction.Run2 import Branch
from PidCalibProduction.Run2 import TupleConfig
from PidCalibProduction import StandardOfflineRequirements as StdCut

##################################################
###      C O N F I G U R A T I O N   1     #######
###. . . . . . . . . . . . . . . . . . . . #######
###        Variables by branch type        #######
##################################################
###  List here variables for each TYPE of branch.
### Branches are: "HEAD", the head of the decay chain
###               "INTERMEDIATE", produced and decayed
###               "TRACK", charged basic particle
###               "NEUTRAL", photons or pi0
##################################################

LokiVarsByType = {
    "HEAD": {
        "M": "M"
        ,"M_DTF" : "DTF_FUN(M,False, strings(['eta_prime']))"
    },
    "INTERMEDIATE": {
        "M": "M"
        ,"M12": "M12"
        ,"M13": "M13"
        ,"M23": "M23"
    },
    "TRACK": {
        "PT"      : "PT"
        ,"Q"      : "Q"
        ,"PROBNNk"      : "PROBNNk"
        ,"PROBNNpi"     : "PROBNNpi"
    },
    "NEUTRAL": {
        "PT": "PT"
        , "P": "P"
        , "ETA": "ETA"
        , "PHI": "PHI"
        , "CL": "CL"
        , "IsNotH": "PPINFO(LHCb.ProtoParticle.IsNotH, -9999.)"
        , "IsNotE": "PPINFO(LHCb.ProtoParticle.IsNotE, -9999.)"
        , "IsPhoton": "PPINFO(LHCb.ProtoParticle.IsPhoton, -9999.)"
        , "CaloNeutralID": "PPINFO(LHCb.ProtoParticle.CaloNeutralID, -9999.)"
        , "CaloNeutralSpd": "PPINFO(LHCb.ProtoParticle.CaloNeutralSpd, -9999.)"
        , "CaloNeutralPrs": "PPINFO(LHCb.ProtoParticle.CaloNeutralPrs, -9999.)"
        , "CaloNeutralEcal": "PPINFO(LHCb.ProtoParticle.CaloNeutralEcal, -9999.)"
        , "sWeight": "WEIGHT"
    }
}

EventInfo = {
    'VOID': {
              "nPVs_Brunel"              : "RECSUMMARY( LHCb.RecSummary.nPVs              , -9999, '/Event/Rec/Summary', False)"
             ,"nTracks_Brunel"           : "RECSUMMARY( LHCb.RecSummary.nTracks           , -9999, '/Event/Rec/Summary', False)"
             ,"nSPDhits_Brunel"          : "RECSUMMARY( LHCb.RecSummary.nSPDhits          , -9999, '/Event/Rec/Summary', False)"
             ,"nPVs"                     : "RECSUMMARY( LHCb.RecSummary.nPVs              , -9999, '/Event/Turbo/Rec/Summary', False)"
             ,"nTracks"                  : "RECSUMMARY( LHCb.RecSummary.nTracks           , -9999, '/Event/Turbo/Rec/Summary', False)"
             ,"nSPDhits"                 : "RECSUMMARY( LHCb.RecSummary.nSPDhits          , -9999, '/Event/Turbo/Rec/Summary', False)"
    },
    "ODIN": {
        "runNumber"      : "ODIN_RUN"
        , "eventNumber1" : "ODIN_EVT1 ( 1000000000L )"
        , "eventNumber2" : "ODIN_EVT2 ( 1000000000L )"
        , "eventNumber"  : " ODIN_EVT1 ( 1000000000L ) + 1000000000L * ODIN_EVT2 ( 1000000000L )"
        , "TCK"          : "ODIN_TCK"
    }
}

##################################################
###      C O N F I G U R A T I O N   2     #######
###. . . . . . . . . . . . . . . . . . . . #######
###        Variables by branch name        #######
##################################################
###  List here variables for each Branch name
###   The purpose of this configuration is to allow
###   to set a variable for all "Lambda_c+" as long
###   as all Lambda_c+ will be named "Lc"
##################################################

LokiVarsByName = {

}

##################################################
###      C O N F I G U R A T I O N   3     #######
###. . . . . . . . . . . . . . . . . . . . #######
###        Filter definition               #######
##################################################
### Filters used to define the various nTuples
##################################################

class FilterCut:
    def __init__(self, cut):
        self.cut = "(" + cut + ")"
    def __add__(self, cut):
        ret = FilterCut(self.cut)
        if isinstance(cut, str):
            ret.cut += "& ( " + cut + ")"
        else:
            ret.cut += "& ( " + cut.cut + ")"
        return ret
    def printout(self):
        print "self.cut = %s" % self.cut



##################################################
###      C O N F I G U R A T I O N   4     #######
###. . . . . . . . . . . . . . . . . . . . #######
### Configuration of the decay structures  #######
##################################################
### List of all the decays, input stripping line,
###  decay structure, and its branch
### TupleConfig and Branch are tricks to ensure
### all the mandatory entries are set or to
### raise an exception here in the configuration
### in case they are not.
### The keywork "Type" allows to set the Type of
### the branch in order to inherit the proper set
### of Loki variables as configured in
### "Configuration1".
### The Name of the branch as defined in the
### dictionary is used to inherit variables as
### defined in configuration2.
### Finally, the keyword "isAlso" allows to set
### other inheritance from configuration2.
### For example, the two muons from the J/psi
### are named mup and mum, but you want both to
### inherit from "mu" since they are muons.
##################################################

tupleConfiguration = {

    #============ Photons  ===============================
    "Dsst2DsGamma":
    TupleConfig(
        Decay="[D*_s+]cc -> [D_s+]cc gamma",
        InputLines=["Hlt2CaloPIDDsst2DsGammaTurboCalib"],
        Calibration="Dsst2DsGamma",
        Filter="",
        Branches={"Dsst" : Branch("[D*_s+]cc", Type='H')}),

    "D2EtapPi":
    TupleConfig(
        Decay="[D+]cc -> eta_prime [pi+]cc",
        InputLines=["Hlt2CaloPIDD2EtapPiTurboCalib"],
        Calibration="D2EtapPi",
        Filter="",
        Branches={"Ds"     : Branch("[D+]cc", Type='H')}),

    "Eta2MuMuGamma":
    TupleConfig(
        Decay="eta -> KS0 gamma",
        InputLines=["Hlt2CaloPIDEta2MuMuGammaTurboCalib"],
        Calibration="Eta2MuMuGamma",
        Filter="",
        Branches={"eta"  : Branch("eta", Type='H')}),

    "B2KstGamma":
    TupleConfig(
        Decay="[B0]cc -> [K*(892)0]cc gamma",
        InputLines="Hlt2CaloPIDBd2KstGammaTurboCalib",
        Calibration="B2KstGamma",
        Filter="",
        Branches={"B0"   : Branch("[B0]cc", Type="H")}),

    "Bs2PhiGamma":
    TupleConfig(
        Decay="B_s0 -> phi(1020) gamma",
        InputLines=["Hlt2CaloPIDBs2PhiGammaTurboCalib"],
        Calibration="Bs2PhiGamma",
        Filter="",
        Branches={"Bs": Branch("B_s0", Type='H')}),

    #============ Pi0  ===============================
    "D02KPiPi0ResolvedPhoton":
    TupleConfig(
        Decay="[D*(2010)+ -> D0 pi+]CC",
        InputLines=["Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalib"],
        Calibration="D02KPiPi0Resolved",
        Filter="",
        Branches={"Dst"    : Branch("[D*(2010)+]CC", Type='H')}),
    
    "D02KPiPi0Merged":
    TupleConfig(
        Decay="[D*(2010)+ -> D0 pi+]CC",
        InputLines=["Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalib"],
        Calibration="D02KPiPi0Merged",
        Filter="",
        Branches={"Dst"    : Branch("[D*(2010)+]CC", Type='H')})
}


from Configurables import MessageSvc
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

dv = DaVinci(
    Turbo           = True,
    RootInTES       = "/Event/Turbo",
    InputType="DST",
    DataType='2018',
    EvtMax=-1)


dv.appendToMainSequence (
    parseConfigurationNeutral( tupleConfiguration
                               , tesFormat  = "<line>/Particles"
                               , mdstOutputFile = "PIDCALIBNEUTRAL" # shouldn't change
                               , mdstOutputPrefix = "" # SHOULD CHANGE!!!
                               , varsByType = LokiVarsByType
                               , varsByName = LokiVarsByName
                               , eventVariables = EventInfo
                               , writeNullWeightCandidates = False
                               , reprocessing = False
                               ))