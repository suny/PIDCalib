###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TeslaTools.TeslaMonitor import TeslaH1, TeslaH2, TeslaH3
from Configurables import DaVinci
from PidCalibProduction import StandardOfflineRequirements as StdCut
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond

Dst2D0PiCut = StdCut.Dst2D0PiCut
Dst2D0PiRCut = StdCut.Dst2D0PiRCut
Dst2D0PiMCut = StdCut.Dst2D0PiMCut
D2EtapPiCut = StdCut.D2EtapPiCut
Eta2MuMuGammaCut = StdCut.Eta2MuMuGammaCut
DsstDsGammaCut = StdCut.DsstDsGammaCut
B2KstGammaCut = StdCut.B2KstGammaCut
Bs2PhiGammaCut = StdCut.Bs2PhiGammaCut

dv = DaVinci()

dv.MoniSequence = [

    #======================== Pi0 =================================
    TeslaH1(
        "Hlt2CaloPIDDstD02KPiPi0_ResolvedPi0TurboCalib:D*(2010)+:M-M1",
        ";m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}#pi^{0}) [MeV/c^{2}]; Candidates",
        560,
        140,
        154,
        Dst2D0PiRCut,
        histname="D02KPiPi0R").getAlgorithm(),
    TeslaH1(
        "Hlt2CaloPIDDstD02KPiPi0_MergedPi0TurboCalib:D*(2010)+:M-M1",
        ";m(D^{0}#pi^{+}) - m(K^{-}#pi^{+}#pi^{0}) [MeV/c^{2}]; Candidates",
        560,
        140,
        154,
        Dst2D0PiMCut,
        histname="D02KPiPi0M").getAlgorithm()

    #======================== Photon =================================
    ,
    TeslaH1(
        "Hlt2CaloPIDD2EtapPiTurboCalib:D+:DTF_FUN(M,False, strings(['eta_prime']))",
        ";m(#eta' #pi) [MeV/c^{2}]; Candidates",
        500,
        1800.,
        2050.,
        D2EtapPiCut,
        histname="D2EtapPi").getAlgorithm(),
    TeslaH1(
        "Hlt2CaloPIDDsst2DsGammaTurboCalib:D*_s+:M-CHILD(1,M)",
        ";m(D_{s} #gamma) - m(D_{s}) [MeV/c^{2}]; Candidates",
        280,
        100,
        260,
        DsstDsGammaCut,
        histname="Dsst2DsGamma").getAlgorithm(),
    TeslaH1(
        "Hlt2CaloPIDEta2MuMuGammaTurboCalib:eta:M",
        ";m(#mu #mu #gamma) [MeV/c^{2}]; Candidates",
        280,
        410,
        690,
        Eta2MuMuGammaCut,
        histname="Eta2MuMuGamma").getAlgorithm(),
    TeslaH1(
        "Hlt2CaloPIDBd2KstGammaTurboCalib:B0:M",
        ";m(K^{*} #gamma) [MeV/c^{2}]; Candidates",
        100,
        4500,
        6500,
        B2KstGammaCut,
        histname="B2KstGamma").getAlgorithm(),
    TeslaH1(
        "Hlt2CaloPIDBs2PhiGammaTurboCalib:B_s0:M",
        ";m(#phi #gamma) [MeV/c^{2}]; Candidates",
        100,
        4500,
        6500,
        Bs2PhiGammaCut,
        histname="Bs2PhiGamma").getAlgorithm()
]
